 
import operator

alldic={}
sdic={}
ts=0
word={}
tw=0

pwdic={}
topdic={}

wordl=5
def lineTokenizeTrain(line):
    global alldic
    global ts
    global tw
    global sdic
    global word
    global wordl
    
    
    subdic={}
    line=line.strip()
    if len(line)>10:
        ls=line.split("@")
        subject=ls[0]
        content=ls[10]
        if subject not in alldic:
            alldic[subject]={}
            sdic[subject]=0
            

        sdic[subject]+=1
        ts+=1
        
        subdic=alldic[subject]
        
        ls=content.split(" ")
        for x in ls:
            if len(x)>wordl:
                tw+=1
                if x not in word:
                    word[x]=0
                word[x]+=1
                
                if x not in subdic:
                   subdic[x]=0
                
                subdic[x]+=1
                   
        alldic[subject]=subdic
                    

def lineTokenizeTest(line):
    global pwdic
    global topdic
    global wordl
    
    
    tw=0
    wtdic={}
    spdic={}
    line=line.strip()
    if len(line)>10:
        ls=line.split("@")
        subject=ls[0]
        content=ls[10]

        
        ls=content.split(" ")
        for x in ls:
            if len(x)>wordl:
               
                tw+=1
                if x not in wtdic:
                   wtdic[x]=0
                
                wtdic[x]+=1
        testdic={}
        
        for k,v in wtdic.items():
            p=v*100/tw

            if k in pwdic:
                tmp=pwdic[k]
                for s in tmp:
                    for m,n in s.items():
                        if m not in spdic:
                            spdic[m]=0
                        spdic[m]+=n*p/topdic[m]

       

        smax=""
        vmax=0
        
        
        
        for k,v in spdic.items():
            if v>vmax:
                smax=k
                vmax=v
 
        return(subject,smax,vmax)    

                    
        
        
            
        
                    
        
              




        

fp=open("HAM-Train.txt", encoding='utf-8')
line=fp.readline()
i=1
while line :
    lineTokenizeTrain(line)
    i+=1
    line=fp.readline()
fp.close()


for k,v in sdic.items():
    print(k,v,ts,v*100/ts)
    topdic[k]=v*100/ts

mdic={}
for k,v in sdic.items():
    for m,n in sdic.items():
        mdic[k+":"+m]=0




print(tw)
fp=open("word-count-uni-train.csv", "w",encoding='utf-8')
fp.write("topic,counttopic,percent-topic,word,count in topic,wordsintopic,Percent-in-topic,count-in-all-topic,total-word,percent-in-total\n")

for k,v in alldic.items():
    l=len(v)
  
    for x,y in v.items():
        p=y*100/l
        pw=word[x]*100/tw
        if x not in pwdic:
            pwdic[x]=[]
        tmp={}
        tmp[k]=p
        pwdic[x].append(tmp)

        fp.write(k+","+str(sdic[k])+","+str(sdic[k]*100/ts)+","+x+","+str(y)+","+str(l)+","+str(p)+","+str(word[x])+","+str(tw)+","+str(pw)+"\n")
fp.close()




print("==============="*5)
fp=open("custom_test_reversed.txt", encoding='utf-8')
line=fp.readline()
i=1
match=0
tot=0
fpw=open("word-count-uni-test.csv", "w",encoding='utf-8')
fpw.write("org-topic,pridic-topic,percent\n")
while line:
    a,b,c=lineTokenizeTest(line)
    if a==b :
        match+=1
    tot+=1
    mdic[a+":"+b]+=1
    
    fpw.write(a+","+b+","+str(c)+"\n")
    i+=1
    line=fp.readline()
fp.close()
fpw.close()
print(tot,match,100*match/tot)

print("------------------------")



fpw=open("word-count-uni-class.csv", "w",encoding='utf-8')

for k,v in sdic.items():
    s=""
    for m,n in mdic.items():
        ls=m.split(":")
        if k in ls[0]:
            s=s+","+m+","+str(n)
    s=s+"\n"
    fpw.write(s)
fpw.close()

